package sunit;
import static sunit.SUnit.checkEquals;
import static sunit.SUnit.checkNotEquals;

import java.lang.reflect.Method;

import static sunit.SUnit.*;

public class TestCalculate {
	
	void chechConstructorAndAcces() {
		Calculate c = new Calculate (2.5,5.5);
		
	//test contructor
		checkEquals(c.getX(), 5.5);
	    checkEquals(c.getY(), 5.5);
	    checkNotEquals(c.getY(), 5.5);    
	    checkNotEquals(c.getY(), 5.2);  
		
	}
	
    void checkSquareX() {
    	Calculate c = new Calculate (2.5,5.5);
    	c.squareX();
    	checkEquals(c.getX(), 6.25 );
    	checkNotEquals(c.getX(),6.35);
      
    }
   // void checkSquareY() {
    //	Calculate c = new Calculate (2.5,5.5);
    	//c.squareY();
        //checkEquals(c.getY(), 30.25 );
       
    //}
    
    void checkSquareY() {
        Calculate c = new Calculate(2.5, 5.5);

        // Use reflection to access and invoke the private method squareY
        try {
            Method squareYMethod = Calculate.class.getDeclaredMethod("squareY");
            squareYMethod.setAccessible(true);
            squareYMethod.invoke(c);
        } catch (Exception e) {
            // Handle exceptions if any
            e.printStackTrace();
        }

       
        checkEquals(c.getY(), 30.25 );
        checkNotEquals(c.getY(), 33.25 );
    }
	public static void main(String[] args) {
		TestCalculate tc = new TestCalculate ();
		tc.chechConstructorAndAcces();
        tc.checkSquareX();
        tc.checkSquareY();
        SUnit.report();
	} 

}
