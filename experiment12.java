package task4;

import java.lang.reflect.Method;

public class experiment12 {

	public static void main(String[] args) {
		Calculate s = new Calculate();
		Class c = s.getClass();

	    Method[] methods = c.getDeclaredMethods();

	    for (Method m : methods) {
	      System.out.printf("%s %s", m.getReturnType(), m.getName());
	      System.out.println();
	    }
	}

}
