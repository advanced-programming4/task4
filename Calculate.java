package task4;

public class Calculate {
	public static double x = 2.5;
	private static  double y = 5.5;
	
	public Calculate() {
		
	}
	
	public Calculate (double x, double y) {
	    this.x = x;
	    this.y = y;
	  }

	  public void squareX() {
	    this.x *= this.x;
	  }

	  @SuppressWarnings("unused")
	private void squareY() {
	    this.y *= this.y;
	  }

	  public double getX() {
	    return x;
	  }

	
	@SuppressWarnings("unused")
	private void setX(double x) {
	    this.x = x;
	  }

	  public double getY() {
	    return y;
	  }

	  public void setY(double y) {
	    this.y = y;
	  }

	  
	  static int returnEight() {
		    return 8;
		  }

		  public static int returnNine() {
		    return 9;
		  }
	  public String toString() {
	    return String.format("(x:%f, y:%f)", x, y);
	  } 
	
			

	public void main(String[] args) {
		
		
		System.out.println("x=" + x);
		System.out.println("y=" + y);
		
		squareX();
		

	}

}
