package task4;

import java.lang.reflect.Field;

public class experiment7 {

	public static void main(String[] args) throws Exception{
		 Calculate s = new Calculate();
		    Field[] fields = s.getClass().getDeclaredFields();
		    System.out.printf("There are %d fields\n", fields.length);

		    for (Field f : fields) {
		      f.setAccessible(true);
		      System.out.printf("field name=%s type=%s value=%f\n", f.getName(),
		          f.getType(), f.getDouble(s));
		    }

	}

}
