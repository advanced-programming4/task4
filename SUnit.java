package sunit;

import java.lang.reflect.Method;
import java.util.*;
import java.lang.reflect.Field;	
import java.util.logging.Level;
import java.util.logging.Logger;

public class SUnit {
	
	  private static final Logger logger = Logger.getLogger(SUnit.class.getName());
	  private static List<String> checks;
	  private static double checksMade = 0.0;
	  private static double passedChecks = 0.0;
	  private static double failedChecks = 0.0;
	  
	  //logging
	  static {
	        // Configure the logger
	        logger.setLevel(Level.INFO);
	    }
	  
	  
	  private static void addToReport(String txt) {
		    if (checks == null) {
		      checks = new LinkedList<String>();
		    }
		    checks.add(String.format("%04.0f: %s", checksMade++, txt));
		  }

	  
	  public static void checkEquals(double value1, double value2) {
		    if (value1 == value2) {
		      addToReport(String.format("  %f == %f", value1, value2));
		      passedChecks++;
		    } else {
		      addToReport(String.format("* %f == %f", value1, value2));
		      failedChecks++;
		    }
		    logger.info("CheckEquals: " + value1 + " == " + value2);
		  }
	  
	  public static void checkNotEquals (double value1, double value2) {
		    if (value1 != value2) {
		      addToReport(String.format("  %f != %f", value1, value2));
		      passedChecks++;
		    } else {
		      addToReport(String.format("* %f != %f", value1, value2));
		      failedChecks++;
		    }
		  }


	  
	  
	  
	  //Test for String
	  
	  public static void checkEquals1(String value1, String value2) {
	        if (value1.equals(value2)) {
	            addToReport(String.format("  \"%s\" == \"%s\"", value1, value2));
	            passedChecks++;
	        } else {
	            addToReport(String.format("* \"%s\" == \"%s\"", value1, value2));
	            failedChecks++;
	        }
	        logger.info("CheckEquals: " + value1 + " == " + value2);
	    }
	  public static void checkNotEquals1(String value1, String value2) {
		    if (value1 != value2) {
		      addToReport(String.format("  %s != %s", value1, value2));
		      passedChecks++;
		    } else {
		      addToReport(String.format("* %s != %s", value1, value2));
		      failedChecks++;
		    }
		  }
	  //Test for Float
	  public static void checkEquals2(Float
			  	value1, Float value2) {
	        if (value1.equals(value2)) {
	            addToReport(String.format("  \"%f\" == \"%f\"", value1, value2));
	            passedChecks++;
	        } else {
	            addToReport(String.format("* \"%f\" == \"%f\"", value1, value2));
	            failedChecks++;
	        }
	        logger.info("CheckEquals: " + value1 + " == " + value2);
	    }
	  public static void checkNotEquals2(Float value1, Float value2) {
		    if (value1 != value2) {
		      addToReport(String.format("  %f != %f", value1, value2));
		      passedChecks++;
		    } else {
		      addToReport(String.format("* %f != %f", value1, value2));
		      failedChecks++;
		    }
		    
		  }
	  
	  //•	Complex assertions
	  public static void assertComplexField(String fieldName, Object expectedValue) {
	        try {
	            Field field = SUnit.class.getDeclaredField(fieldName);
	            field.setAccessible(true); // Allow access to private fields
	            Object actualValue = field.get(null); // null for static fields

	            if (expectedValue.equals(actualValue)) {
	                addToReport(String.format("  %s == %s", fieldName, expectedValue));
	                passedChecks++;
	            } else {
	                addToReport(String.format("* %s == %s", fieldName, expectedValue));
	            }
	        } catch (Exception e) {
	            handleException("assertComplexField", e);
	        }
	    }
	  public static void assertComplexMethod(String methodName, Object expectedValue) {
	        try {
	            Method method = SUnit.class.getDeclaredMethod(methodName);
	            method.setAccessible(true); // Allow access to private methods
	            Object actualValue = method.invoke(null); // null for static methods

	            if (expectedValue.equals(actualValue)) {
	                addToReport(String.format("  %s() == %s", methodName, expectedValue));
	                passedChecks++;
	            } else {
	                addToReport(String.format("* %s() == %s", methodName, expectedValue));
	                failedChecks++;
	            }
	        } catch (Exception e) {
	            handleException("assertComplexMethod", e);
	        }
	    }
	  
	  //Launcher
	  
	  public static void runChecks() {
	        Method[] methods = SUnit.class.getDeclaredMethods();
	        for (Method method : methods) {
	            if (method.getName().startsWith("check")) {
	                try {
	                    method.invoke(null);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	  
	//Resilience to exceptions   
	  
	  @SuppressWarnings("unused")
	private static void handleException(String methodName, Exception e) {
		    // Record the exception in the reports
		    addToReport(String.format("* Test '%s' threw an exception: %s", methodName, e.getMessage()));
		    failedChecks++;
		}
	  
	  
	  public static void report() {
		  //  System.out.printf("%f checks passed\n", passedChecks);
		   // System.out.printf("%f checks failed\n", failedChecks);
		    //System.out.println();
		    
		    //for (String check : checks) {
		      //System.out.println(check);
		  // }
		  logger.info("Total checks passed: " + passedChecks);
	        logger.info("Total checks failed: " + failedChecks);

	        for (String check : checks) {
	            logger.info(check);
	        }
	  }
	  
	public static void main(String[] args) {
 

	}

}
