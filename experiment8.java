package task4;

import java.lang.reflect.Field;

public class experiment8 {

	public static void main(String[] args) throws Exception {
		Calculate c = new Calculate();
	    Field[] fields = c.getClass().getDeclaredFields();
	    System.out.printf("There are %d fields\n", fields.length);
	    for (Field f : fields) {
	      f.setAccessible(true);
	      double x = f.getDouble(c);
	      x++;
	      f.setDouble(c, x);
	      System.out.printf("field name=%s type=%s value=%f\n", f.getName(),
	          f.getType(), f.getDouble(c));
	    }
	}

}
