package task4;

import java.lang.reflect.Field;

public abstract class experiment4 {

	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException {
		 Calculate s = new Calculate();
		    Field[] fields = s.getClass().getFields();
		    System.out.printf("There are %d fields\n", fields.length);
		    for (Field f : fields) {
		      System.out.printf("field name=%s type=%s value=%f\n", f.getName(),
		          f.getType(), f.getDouble(s));

	}

}
}