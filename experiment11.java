package task4;

import java.lang.reflect.Method;



public class experiment11 {

	public static void main(String[] args) {
		Calculate s = new Calculate();
	    Class c = s.getClass();

	    System.out.println("class = " + c);
	    System.out.println("class name = " + c.getName());

	    Method[] methods = c.getDeclaredMethods();

	    for (Method m : methods) {
	      System.out.printf("%s", m.getName());
	      System.out.println();
	    }
	}

}
